﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public static bool gameOver = false;
    public Text distanceText;
    public Text gameOverText;
    private float distance = 0;
    private int highScore = 0;
    public Text highScoreText;
    public Text newHighScoreText;

    void Start()
    {
        gameOver = false;
        distance = 0;
        highScore = PlayerPrefs.GetInt("HighScore", 0);
        distanceText.text = distance.ToString();
        highScoreText.text = "High Score: " + highScore.ToString();
        gameOverText.gameObject.SetActive(false);
        highScoreText.gameObject.SetActive(false);
        newHighScoreText.gameObject.SetActive(false);
    }

    void Update()
    {
        if (gameOver)
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over \n Total Distance: " + distanceText.text.ToString();
            gameOverText.gameObject.SetActive(true);
            highScoreText.gameObject.SetActive(true);

            int currentScore = Mathf.RoundToInt(distance);

            if (currentScore > highScore)
            {
                highScore = currentScore;
                PlayerPrefs.SetInt("HighScore", highScore);
                highScoreText.text = "High Score: " + highScore.ToString();

                // Activa el texto de nuevo record
                newHighScoreText.gameObject.SetActive(true);
                newHighScoreText.text = "Hiciste un nuevo record!";
            }
        }
        else
        {
            distance += Time.deltaTime;
            distanceText.text = Mathf.RoundToInt(distance).ToString();
        }
    }
}
