﻿using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    public static float enemyVelocity;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Controller_Player.vida == 0)
        {
            Destroy(this.gameObject);
        }
        rb.AddForce(new Vector3(-enemyVelocity, 0, 0));
        OutOfBounds();
    }

    public void OutOfBounds()
    {
        if (this.transform.position.x <= -20)
        {
            Destroy(this.gameObject);
        }
    }
}
