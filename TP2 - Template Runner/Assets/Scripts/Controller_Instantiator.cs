﻿using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public List<GameObject> enemies;
    public List<GameObject> coinPrefabs;
    public GameObject powerUpPrefab;
    public GameObject instantiatePos;
    public GameObject reloj;
    public int cantidadCoins;
    public float respawningTimer;
    public float cantidadCoinsInterval;
    private float coinTimer;
    private float maxCoinCount = 1000f;
    private float powerUpTimer = 0f;
    private float time = 0;
    private bool powerUpGenerated = false;
    private List<Vector3> usedPositions = new List<Vector3>();
    void Start()
    {
        Controller_Enemy.enemyVelocity = 2;
    }

    void Update()
    {
        SpawnCoins();
        SpawnEnemies();
        ChangeVelocity();
        GeneratePowerUp();

        if (powerUpTimer >= Random.Range(7f, 15f))
        {
            powerUpGenerated = false;
        }
    }

    void SpawnCoins()
    {
        if (GameObject.FindGameObjectsWithTag("coin").Length >= maxCoinCount) return;

        coinTimer += Time.deltaTime;
        if (coinTimer >= cantidadCoinsInterval)
        {
            GameObject coinPrefab = coinPrefabs[Random.Range(0, coinPrefabs.Count)];
            for (int i = 0; i < cantidadCoins; i++)
            {
                // Generar la posición de la moneda, evitando posiciones ya usadas
                Vector3 coinPos;
                do
                {
                    coinPos = new Vector3(transform.position.x + i * cantidadCoinsInterval, Random.Range(0f, 11f), transform.position.z);
                } while (usedPositions.Contains(coinPos));
                usedPositions.Add(coinPos);

                GameObject coin = Instantiate(coinPrefab, coinPos, Quaternion.identity);

                // Actualizar la posición para la siguiente iteración
                coinPos += Vector3.right * cantidadCoinsInterval;
                transform.position = coinPos;

                break; // Salir del bucle después de generar un objeto
            }
            coinTimer = 0;
        }
    }

    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            // Generar la posición del enemigo, evitando posiciones ya usadas
            Vector3 instantiatePosWithY;
            do
            {
                instantiatePosWithY = new Vector3(instantiatePos.transform.position.x, Random.Range(0f, 11f), instantiatePos.transform.position.z);
            } while (usedPositions.Contains(instantiatePosWithY));
            usedPositions.Add(instantiatePosWithY);

            Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Count)], instantiatePosWithY, Quaternion.identity);

            respawningTimer = UnityEngine.Random.Range(.2f, 1);
        }
    }

    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 10f, time / 200f);
    }

    private void GeneratePowerUp()
    {
        if (!powerUpGenerated)
        {
            powerUpTimer += Time.deltaTime;

            if (powerUpTimer >= Random.Range(7f, 15f))
            {
                if (Random.value < 0.5f)
                {
                    Instantiate(powerUpPrefab, new Vector3(10f, 5f, 0f), Quaternion.identity);
                }
                else
                {
                    Instantiate(reloj, new Vector3(10f, 5f, 0f), Quaternion.identity);
                }

                powerUpGenerated = true;
                powerUpTimer = 0f; // reiniciar el temporizador
            }
        }
    }
}
