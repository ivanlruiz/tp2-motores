using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PowerUp : MonoBehaviour
{
    public Slider powerUpDurationSlider; // Referencia al objeto Slider en la escena
    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        rb.velocity = new Vector3(-Controller_Enemy.enemyVelocity * 8, 0, 0);
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Controller_Player player = collision.gameObject.GetComponent<Controller_Player>();

            // Llamamos al m�todo ActivatePowerUp del jugador y le pasamos la duraci�n del PowerUp
            player.ActivatePowerUp(5f);

            StartCoroutine(ShowPowerUpDuration()); // Corutina que muestra el Slider por 5 segundos

            Destroy(this.gameObject);
        }
    }

    private IEnumerator ShowPowerUpDuration()
    {
        powerUpDurationSlider.gameObject.SetActive(true); // Activamos el objeto Slider
        powerUpDurationSlider.value = 1f; // Establecemos el valor inicial del Slider en 1

        float timeLeft = 5f; // Establecemos el tiempo restante en 5 segundos
        while (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime; // Restamos el tiempo transcurrido desde el �ltimo frame
            powerUpDurationSlider.value = timeLeft / 5f; // Actualizamos el valor del Slider
            yield return null; // Esperamos al siguiente frame
        }

        powerUpDurationSlider.gameObject.SetActive(false); // Desactivamos el objeto Slider
        Controller_Player.timeBetweenShots = 0.1f; // Establecemos el tiempo entre disparos en 0.1 segundos
    }
}
