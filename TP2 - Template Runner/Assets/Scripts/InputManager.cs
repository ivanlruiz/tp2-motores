using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance { get; private set; }
    public event System.Action<float> OnHorizontalInput;
    public event System.Action<float> OnVerticalInput;
    public event System.Action OnJumpInput;
    public event System.Action OnFireInput;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    private void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        if (horizontal != 0f)
        {
            OnHorizontalInput?.Invoke(horizontal);
        }

        if (vertical != 0f)
        {
            OnVerticalInput?.Invoke(vertical);
        }

        if (Input.GetButtonDown("Jump"))
        {
            OnJumpInput?.Invoke();
        }

        if (Input.GetButtonDown("Fire1"))
        {
            OnFireInput?.Invoke();
        }
    }
}

