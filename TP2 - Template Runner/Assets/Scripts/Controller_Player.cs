﻿using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.ParticleSystem;

public class Controller_Player : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] private float moveSpeed = 10f;
    private Rigidbody rb;

    [Header("Health")]
    public static float vida = 1f;

    [Header("Flying")]
    [SerializeField] private float flyForce = 100f;
    [SerializeField] private float gravityModifier = 2f;
    private bool isFlying = false;

    [Header("Shooting")]
    [SerializeField] private GameObject bala;
    [SerializeField] private float shootSpeed;
    private bool isShooting = false;
    public static float timeBetweenShots = .2f;
    private float lastShotTime = 0f;

    [Header("Power-Up")]
    [SerializeField] private Slider powerUpSlider;
    private bool powerUpActive = false;
    private float powerUpDuration = 0f;
    private float powerUpTimeRemaining = 0f;

    [Header("Particles")]
    [SerializeField] private GameObject particles;
    private ParticleSystem rocketParticles;

    [Header("Game Duration")]
    [SerializeField] private Slider slider;
    [SerializeField] private float gameDuration = 10f;
    public static float timeLeft;
    private void Start()
    {
        powerUpSlider.gameObject.SetActive(false);
        vida = 1;
        rb = GetComponent<Rigidbody>();

        // Inicializar la barra de progreso
        slider.maxValue = gameDuration;
        slider.value = gameDuration;
        timeLeft = gameDuration;

        // Desactivar la gravedad
        rb.useGravity = false;

        // Ajustar el multiplicador de gravedad
        Physics.gravity *= gravityModifier;

        rocketParticles = particles.GetComponent<ParticleSystem>();

        rocketParticles.Stop();
    }

    private void Update()
    {
        GetInput();

        timeLeft -= Time.deltaTime;
        slider.value = timeLeft;

        if (timeLeft <= 0)
        {
            Controller_Hud.gameOver = true;
            Destroy(this.gameObject);
        }

        if (vida == 0)
        {
            Destroy(this.gameObject);
        }

        

        if (powerUpActive)
        {
            powerUpTimeRemaining -= Time.deltaTime;
            powerUpSlider.value = powerUpTimeRemaining / powerUpDuration;

            if (powerUpTimeRemaining <= 0)
            {
                powerUpActive = false;
                timeBetweenShots = 0.2f;
                powerUpSlider.gameObject.SetActive(false);
            }
        }

    }

    private void GetInput()
    {
        if (Input.GetButton("Fire1") && Time.time > lastShotTime + timeBetweenShots)
        {
            isShooting = true;
            lastShotTime = Time.time;
            Shoot();
        }
        else
        {
            isShooting = false;
        }

        if (Input.GetKey(KeyCode.Space))
        {
            isFlying = true;

            // Calcular la velocidad de vuelo interpolando suavemente entre la velocidad actual y la velocidad de vuelo deseada
            float targetVelocityY = Mathf.Lerp(rb.velocity.y, flyForce, Time.deltaTime * 5f);
            rb.velocity = new Vector3(rb.velocity.x, targetVelocityY, rb.velocity.z);

            rocketParticles.Play();
        }
        else
        {
            isFlying = false;
            rocketParticles.Stop();
        }
    }

    public void ActivatePowerUp(float duration)
    {
        powerUpDuration = duration;
        powerUpTimeRemaining = duration;
        powerUpActive = true;
        timeBetweenShots = 0.05f;
        powerUpSlider.gameObject.SetActive(true);
    }

    private void FixedUpdate()
    {
        if (!isFlying)
        {
            Fall();
        }
        Move();
    }

    private void Move()
    {
        float moveY = 0f;
        if (isFlying)
        {
            moveY = Input.GetAxisRaw("Vertical");
        }
        Vector3 moveDirection = new Vector3(0f, moveY, 0f);
        rb.MovePosition(transform.position + moveDirection * moveSpeed * Time.fixedDeltaTime);
    }

    private void Fall()
    {
        rb.AddForce(Physics.gravity * rb.mass);
    }

    public void Shoot()
    {
        // Crear una instancia del prefab del cubo en la posición del jugador
        GameObject cube = Instantiate(bala, transform.position, Quaternion.identity);

        // Acceder al Rigidbody del cubo y establecer su velocidad en el eje x
        Rigidbody cubeRb = cube.GetComponent<Rigidbody>();
        cubeRb.velocity = new Vector3(shootSpeed, 0f, 0f);
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            vida = 0;
            Controller_Hud.gameOver = true;
        }
    }
}
