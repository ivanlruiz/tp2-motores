using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    private Rigidbody rb;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();

    }
    private void Update()
    {


        rb.velocity = new Vector3(-Controller_Enemy.enemyVelocity*8, 0, 0);
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Controller_Player.timeLeft += 1;
            if (Controller_Player.timeLeft >= 10)
            {
                Controller_Player.timeLeft = 10;
            }

            Destroy(this.gameObject);
        }
    }
}
