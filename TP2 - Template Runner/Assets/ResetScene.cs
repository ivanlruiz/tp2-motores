using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetScene : MonoBehaviour
{
    private Vector3 startPos;
    private Quaternion startRot;
    private GameObject[] objectsToReset;

    private void Start()
    {
        startPos = transform.position;
        startRot = transform.rotation;
        objectsToReset = GameObject.FindGameObjectsWithTag("ResetOnSceneReset");
    }

    public void ResetSceneObjects()
    {
        // Reset player position and rotation
        transform.position = startPos;
        transform.rotation = startRot;

        // Reset other objects with tag "ResetOnSceneReset"
        foreach (GameObject obj in objectsToReset)
        {
            obj.SendMessage("ResetObject");
        }
    }
}
